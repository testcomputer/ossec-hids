
# OSSEC v3.7.0 Installation Guide

---

**Copyright (C) 2019 Trend Micro Inc.**

---

## Introduction to OSSEC

OSSEC is a comprehensive open-source host-based intrusion detection system (HIDS) that offers log analysis, integrity checking, and more. As a powerful security tool, OSSEC provides real-time alerting and response to assist administrators in detecting and mitigating threats. 

For an in-depth exploration of OSSEC's features, visit [OSSEC's official website](https://www.ossec.net). Additionally, the [OSSEC documentation](https://documentation.wazuh.com/current/user-manual/index.html) provides a wealth of knowledge for users and administrators.

---

## Standard Installation

This approach is recommended for most users, especially those unfamiliar with OSSEC's internal structure.

### Prerequisites

Before proceeding with the installation, it's essential to ensure that your system has the required libraries and tools. These dependencies are critical for OSSEC's functionality:

- Libraries:
  - `libssl`: Provides cryptographic capabilities.
  - `libpcre2`: Used for regular expression matching.
  - `libz`: Offers compression functions.
- Development Tools:
  - `make`: Build automation tool.
  - `gcc`: GNU Compiler Collection.
  - `libsystemd-dev`: Development files for systemd.

For Ubuntu/Debian systems, these dependencies can be installed using the following command:

```bash
apt update
apt install libz-dev libssl-dev libpcre2-dev build-essential libsystemd-dev
```

### Installation Procedure:

1. **Script Execution**: Start the installation by running the `install.sh` script. This script has been tailored to guide users through the necessary steps, making the process intuitive.
   
   ```bash
   ./install.sh
   ```

2. **Directory Configuration**: The script will establish `/var/ossec` as the default directory. Additionally, it will try to integrate the initialization script into your system's startup sequence. In the event that this automated integration fails, you can manually include OSSEC in your system's startup sequence. To initiate OSSEC manually:

   ```bash
   /var/ossec/bin/ossec-control start
   ```

3. **Server-Client Architecture**: If you're deploying OSSEC across multiple machines, it's imperative to set up the server component first. The `manage_agents` utility is instrumental in managing agent keys, as demonstrated below:

   ```bash
   /var/ossec/bin/manage_agents
   ```

4. **Finalization**: Once installation is complete, OSSEC will actively monitor your system, providing real-time security insights.

---

## Advanced Manual Installation

For experienced users or developers seeking a granular installation process.

### Steps:

1. **Directory Creation**: Establish the primary directories, with `/var/ossec` being the default:

   ```bash
   mkdir /var/ossec
   ```

2. **File Transfer**: Migrate essential files to the ossec directory.
   
   ```bash
   cp -r /path/to/ossec-source/* /var/ossec/
   ```

3. **Compilation**: Initiate the compilation procedure.
   
   ```bash
   make TARGET=server
   ```

4. **Binary Relocation**: Shift the compiled binaries to the default directory.

5. **User Management**: Create the necessary user profiles to manage and run OSSEC services.

6. **Permission Management**: Ensure the appropriate file permissions are set, safeguarding sensitive configurations and logs.

**Configuration Note**: The Makefile extracts configuration options from the `LOCATION` file. Modify this file to suit your specific needs.

To manually oversee the entire compilation:

```bash
make clean
make all
su
make server
```

**Critical Reminder**: Before executing `make server`, ensure that the required user profiles exist within your system. This is a step the Makefile won't undertake on your behalf.

---

**We appreciate your decision to integrate OSSEC into your security framework. Wishing you a secure and resilient digital experience.**

For further support or inquiries, consider joining the [OSSEC community forums](https://groups.google.com/forum/#!forum/ossec-list) or checking the [FAQ section](https://www.ossec.net/docs/faq/index.html) on the official website.

