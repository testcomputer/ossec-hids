
# OSSEC v3.7.0
### Copyright (C) 2019 Trend Micro Inc.

---

## 🌐 About OSSEC
For more information about OSSEC, please visit [OSSEC's official website](https://www.ossec.net).

---

## 🚀 Recommended Installation

Installing OSSEC is a breeze. For most users, the **Fast Way** is the way to go! However, if you're an experienced user or a developer, you can opt for a custom installation.

### Prerequisites
Ensure your system has the required libraries and tools:
- `libssl`
- `libpcre2`
- `libz`
- `make`, `gcc`
- `libsystemd-dev`

For Ubuntu/Debian users, you can install these with:
```bash
apt install libz-dev libssl-dev libpcre2-dev build-essential libsystemd-dev
```

### Fast Way Installation:
1. Execute the script `./install.sh`. It will walk you through the installation.
2. By default, everything will be created in `/var/ossec`. The script will attempt to set up the initialization script on your system (either in `/etc/rc.local` or `/etc/rc.d/init.d/ossec`). If the init script isn't set up, follow the instructions from `install.sh` to ensure OSSEC starts on boot. To start manually, execute `/var/ossec/bin/ossec-control start`.
3. If setting up on multiple clients, ensure you install the server first. Use the `manage_agents` tool for the correct encryption keys.
4. Sit back, relax, and enjoy your fortified system!

---

## 🛠 Manual Installation (Advanced Users Only!)

1. Create the necessary directories (by default `/var/ossec`).
2. Transfer the essential files to the ossec directory.
3. Begin the compilation process.
4. Move the binaries to the default directory.
5. Generate the required users.
6. Assign the appropriate file permissions.

These steps are encapsulated in the Makefile (refer to `make server`).

**Note**: The Makefile fetches options from the `LOCATION` file. Modify as needed.

To handle the compilation yourself:
```bash
make clean
make all
su
make server
```
**Important**: Before running `make server`, ensure the necessary users are present. The Makefile will not handle user creation.

---

**Thank you for choosing OSSEC!**
